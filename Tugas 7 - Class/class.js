// Soal 1. Animal Class
// release 0
console.log("\n Soal no 1 \n")
class Animal {
    // Code class di sini
    constructor(name,legs=4,cold_blooded=false){
        this.name=name;
        this.legs=legs;
        this.cold_blooded=cold_blooded;
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

// release 1
class Ape extends Animal{
    constructor(name,legs=2,cold_blooded){
        super(name,legs,cold_blooded)
    }

    yell(){
        console.log("Auooo")
    }
}

class Frog extends Animal{
    constructor(name,legs,cold_blooded){
        super(name,legs,cold_blooded);
    }

    jump(){
        console.log("hop hop")
    }
}
// Code class Ape dan class Frog di sini
 
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 

// Soal 2. Function to Class
console.log("\n Soal no 2 \n")
class Clock{
    timer;
    // template="";
    constructor({template}){
        this.template=template
    }
  render() {
    var date = new Date();

    var hours = date.getHours();
    if (hours < 10) hours = '0' + hours;

    var mins = date.getMinutes();
    if (mins < 10) mins = '0' + mins;

    var secs = date.getSeconds();
    if (secs < 10) secs = '0' + secs;
    var output = this.template
      .replace('h', hours)
      .replace('m', mins)
      .replace('s', secs);
    console.log(output);
  }

  stop () {
    clearInterval(this.timer);
  };

  start(){
    this.render();
    this.timer = setInterval(()=>{
        this.render()}, 1000);
  };
}
// new Clock()

var clock = new Clock({template: 'h:m:s'});
clock.start(); 