import React,{Component} from 'react';
import{
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    FlatList,
    Button,
    
} from 'react-native'
// import {AuthContext}
import Icon from "react-native-vector-icons/MaterialIcons";
export default class AboutScreen extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.navbar}>
                    <Icon name="arrow-back" size={25} onPress={()=>{this.props.navigation.goBack()}}></Icon>
                </View>
                <View style={styles.displayPicture}>
                    <Image source={require("./images/profil.png")}></Image>
                    <Text style={{fontSize:17,fontFamily:'Roboto'}}>Oscar Hasan Putra</Text>
                    <Text style={{fontSize:12,fontFamily:'Roboto',marginTop:20, fontWeight:'100'}}>I`m a React Native Developer</Text>
                    
                </View>
                <View style={{...styles.displayPicture,flexDirection:'row',justifyContent:"center",marginTop:20}}>
                <Image style={{marginHorizontal:5}} source={require('./images/facebook.png')}></Image>
                    <Image style={{marginHorizontal:5}} source={require('./images/twitter.png')}></Image>
                    <Image style={{marginHorizontal:5}} source={require('./images/instagram.png')}></Image>
                </View>
                <View style={styles.BoxBottom}>
                    <View style={{flexDirection:'row',justifyContent:'space-between',marginVertical:15,marginLeft:25}}>
                        <Text>My Portofolio</Text>
                        <Icon name="expand-more" size={30}></Icon>
                    </View>
                    <View style={styles.displayPicture}>
                        <Image source={require("./images/gitlab.png")} ></Image>
                        <Text style={{paddingLeft:25}}>Link: https://gitlab.com/oscarhasanp/sanbercodeimrn0620</Text>
                    </View>
                </View>
            </View>
        )
    }
}

const styles=StyleSheet.create({
    BoxBottom:{
        marginTop:30,
        borderColor:'#F6F0F0',
        alignSelf:'center',
        width:'98%',
        borderWidth:1,
        backgroundColor:'#F6F0F0',
        borderTopStartRadius:30,
        shadowColor: "#000",
shadowOffset: {
	width: 0,
	height: 8,
},
shadowOpacity: 0.8,
shadowRadius: 10.32,
elevation: 16,
        
        borderTopEndRadius:30,
        flex:1
    },
    container:{
        flex:1
    },
    navbar:{
        margin:25,
    },
    displayPicture:{
        alignItems:'center'
    }
})