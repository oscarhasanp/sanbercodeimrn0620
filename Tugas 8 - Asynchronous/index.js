// di index.js
// Soal no 1
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Tulis code untuk memanggil function readBooks di sini
function readSingleBook(waktu,indexBuku=0){
    
    readBooks(waktu,books[indexBuku],(time)=>{
        indexBuku++;
        if(books[indexBuku]){
            readSingleBook(time,indexBuku)
        }
    })
}
readSingleBook(10000);
