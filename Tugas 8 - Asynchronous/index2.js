// Soal no 2

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
function readSingleBook(waktu,indexBuku=0){
    readBooksPromise(waktu,books[indexBuku]).then((time)=>{
        indexBuku++;
        if(books[indexBuku])
            readSingleBook(time,indexBuku)
    }).catch(time=>{

    })
}
readSingleBook(10000);