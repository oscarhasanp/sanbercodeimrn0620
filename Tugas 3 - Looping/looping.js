// No. 1 Looping While
console.log("\n// No. 1 Looping While \n")
var i=2; 

console.log("LOOPING PERTAMA")
while(i<=20){
    console.log(`${i} - I Love Coding`);
    i+=2;
}

i-=2;
console.log("LOOPING KEDUA")
while(i>=2){
    console.log(`${i} - I will become a mobile developer`)
    i-=2;
}

// No. 2 Looping menggunakan for
console.log("\n// No. 2 Looping menggunakan for \n")
for(let j=1;j<=20;j++){
    if(j%2==0){
        console.log(`${j} - Berkualitas`);
    }else{
        if(j%3==0){
            console.log(`${j} - I Love Coding`)
        }else{
            console.log(`${j} - Santai`)
        }
    }
}

// No. 3 Membuat Persegi Panjang 
console.log("\n// No. 3 Membuat Persegi Panjang \n")
for(let j=0;j<4;j++){
    let str="";
    for(let k=0;k<8;k++){
        str+="#";
    }
    console.log(str);
}

// No. 4 Membuat Tangga

console.log("\n// No. 4 Membuat Tangga\n")
for(let j=1;j<=7;j++){
    let str="";
    for(let k=0;k<j;k++){
        str+="#";
    }
    console.log(str);
}

// No. 5 Membuat Papan Catur
console.log("\n// No. 5 Membuat Papan Catur \n")
for(let j=0;j<8;j++){
    let str="";
    for(let k=0;k<8;k++){
        str+=j%2==0?(k%2==0?" ":"#"):(k%2==0?"#":" ");
    }
    console.log(str);
}