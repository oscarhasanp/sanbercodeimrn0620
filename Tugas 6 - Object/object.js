//Soal No. 1 (Array to Object)

console.log("\n Soal no 1 \n");
function arrayToObject(arr) {
    if(!arr || arr.length==0){
        console.log("")
        return;
    }
    let objInstant={};
    let year=new Date().getFullYear();
    for(let i=0;i<arr.length;i++){
        let obj={
            firstName: `${arr[i][0]}`,
            lastName: `${arr[i][1]}`,
            gender: `${arr[i][2]}`,
            age: year<arr[i][3]||isNaN(arr[i][3]*year)?"Invalid birth year":year-arr[i][3]
 
        }
        objInstant[`${arr[i][0]} ${arr[i][1]}`]=obj;
    }
    console.log(objInstant);
    // Code di sini 
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/
 
// Error case 
arrayToObject([]) // ""

//Soal No. 2 (Shopping Time)
console.log("\n Soal No 2 \n")
const listBarang=[{
    barang:"Sepatu Stacattu",
    harga:1500000},
    {barang:"Baju Zoro",
    harga:500000},
    {barang:"Baju H&N",
    harga:250000},
    {barang:"Sweater Uniklooh",
    harga:175000},
    {barang:"Casing Handphone",
    harga:50000}
]
const shoppingTime=(memberId, money)=>{
    listBarang.sort((a,b)=>b.harga-a.harga);
    if(!memberId)
        return "Mohon maaf, toko X hanya berlaku untuk member saja";
    if(money<listBarang[listBarang.length-1].harga)
        return "Mohon maaf, uang tidak cukup"
    else{
        let uang=money;
        let cart=listBarang.filter((barang)=>{
            if(uang>=barang.harga){
                uang-=barang.harga;
                return true;
            }
            return false;         
        }).map((barang)=>{
            return barang.barang;
        });
        
        return {
            memberId:memberId,
            money:money,
            listPurchased:cart,
            changeMoney:uang
        }
    }
}
// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
  //{ memberId: '1820RzKrnWn08',
  // money: 2475000,
  // listPurchased:
  //  [ 'Sepatu Stacattu',
  //    'Baju Zoro',
  //    'Baju H&N',
  //    'Sweater Uniklooh',
  //    'Casing Handphone' ],
  // changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//Soal No. 3 (Naik Angkot)
console.log("\n Soal no 3 \n")
function naikAngkot(arrPenumpang) {
   let rute = ['A', 'B', 'C', 'D', 'E', 'F'];
   return arrPenumpang.map((penumpang)=>{
        let naikDari=rute.indexOf(penumpang[1])
        let tujuan=rute.indexOf(penumpang[2])
        let ruteList=rute.slice(naikDari,tujuan);
        
        let harga=ruteList.length*2000;
        return {
            penumpang:penumpang[0],
            naikDari:penumpang[1],
            tujuan:penumpang[2],
            bayar:harga
        }
   })
    //your code here
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]