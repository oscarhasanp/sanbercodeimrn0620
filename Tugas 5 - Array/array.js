//Soal No. 1 (Range)

const range=(startNum,finishNum)=>{
    if(!startNum||!finishNum)
        return -1;
    else{
        let strArray=[];
        strArray.push(startNum);
        while(startNum<finishNum || startNum>finishNum){
            startNum=startNum<finishNum?(startNum+1):(startNum-1);
            strArray.push(startNum);
        }
        return strArray;
    }
}

console.log("\n\n // Soal No. 1 \n ")
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 


// Soal No. 2 (Range with Step)
const rangeWithStep=(startNum, finishNum, step)=>{
    if(!startNum||!finishNum)
        return -1;
    else{
        let strArray=[];
        strArray.push(startNum);
        while(startNum<=(finishNum-step) || startNum>=(finishNum+step)){
            startNum=startNum<finishNum?(startNum+step):(startNum-step);
            strArray.push(startNum);
        }
        return strArray;
    }
}

console.log("\n\n // Soal No. 2 \n")
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5]


//Soal No. 3 (Sum of Range)
const sum=(startNum,finishNum,step=1)=>{
    let jumlah=0;
    jumlah+=startNum;
    while(startNum<=(finishNum-step) || startNum>=(finishNum+step)){
        startNum=startNum<finishNum?(startNum+step):(startNum-step);
        jumlah+=startNum;
    }
    return isNaN(jumlah)?0:jumlah;
}

console.log("\n\n // Soal No. 3 \n")
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2))// 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

// Soal No. 4 (Array Multidimensi)

const dataHandling=(arrayInput)=>{
    let str="";
    for(let i =0;i<arrayInput.length;i++){
        str+=`\n\n        Nomor ID:  ${arrayInput[i][0]} \n
        Nama Lengkap:  ${arrayInput[i][1]} \n
        TTL:  ${arrayInput[i][2]} \n
        Hobi:  ${arrayInput[i][3]} \n`
    }
    return str;
}

//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] 

console.log("\n\n // Soal No. 4 \n ")
console.log(dataHandling(input))


// Soal No. 5 (Balik Kata)

const balikKata=(str)=>{
    let tempStr=""
    for(let i=str.length-1;i>=0;i--){
        tempStr+=str[i];
    }
    return tempStr;
}

console.log("\n\n // Soal No. 5 \n")
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

// Soal No. 6 (Metode Array)

function dataHandling2(input=[]){
    let newString=input[1]+" Elshawary"
    let newProvinsi="Provinsi "+input[2]
    input.splice(1,1,newString);
    input.splice(2,1,newProvinsi)
    input.pop();
    input.push("Pria", "SMA Internasional Metro")
    let newArray=input;
    let tanggal= newArray.slice(3,4).join()
    let arrayTanggal=tanggal.split("/")
    let bulan= "";
    switch (Number(arrayTanggal[1])) {
        case 1:
            bulan="Januari";
            break;
        case 2:
            bulan="Febuari";
            break;
        case 3:
            bulan="Maret";
            break;
        case 4:
            bulan="April";
            break;
        case 5:
            bulan="Mei";
            break;
        case 6:
            bulan="Juni";
            break;
        case 7:
            bulan="Juli";
            break;
        case 8:
            bulan="Agustus";
            break;
        case 9:
            bulan="September";
            break;
        case 10:
            bulan="Oktober";
            break;
        case 11:
            bulan="November";
            break;
        case 12:
            bulan="Desember";
            break;
        default:
            break;
    }
    let stringArray=arrayTanggal.join("-")
    arrayDescending=arrayTanggal.sort((a,b)=>{
        return b-a;
    })
    let nama=newArray[1].slice(0,15);
    // console.log(arrayTanggal)
    console.log(newArray);
    console.log(bulan);
    console.log(arrayDescending);
    console.log(stringArray);
    console.log(nama);
}
var test=["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
console.log("\n\n // Soal No. 6 \n")
dataHandling2(test)