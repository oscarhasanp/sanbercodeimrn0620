// Soal no 1. Mengubah fungsi menjadi fungsi arrow

console.log("\n Soal No. 1 \n");
const golden = ()=>{
    console.log("this is golden!!")
  }
golden();

// Soal No 2. Sederhanakan menjadi Object literal di ES6
console.log("\n Soal No 2 \n")
const newFunction =(firstName, lastName)=>{
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(`${firstName} ${lastName}`);
        return 
      }
    }
  }
  //Driver Code 
newFunction("William", "Imoh").fullName();

// Soal No 3. Destructuring
console.log("\n Soal No 3 \n")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

  const {firstName,lastName,destination,occupation}= newObject;
  // Driver code
console.log(firstName, lastName, destination, occupation)

// Soal No 4. Array Spreading
console.log("\n Soal No 4 \n")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east];
//Driver Code
console.log(combined)

// Soal No 5. Template Literals
console.log("\n Soal no 5\n")
const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
    
 
// Driver Code
console.log(before) 